<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cloutier\PhpIpfsApi\IPFS;
use File;

class FileController extends Controller
{
    private $ipfs;

    public function __construct(){
        $this->ipfs = new IPFS('localhost', '8080', '5001');
    }

    public function byte2hex($bytes){
        $hexstr = unpack('H*', $bytes);

        return array_shift($hexstr);
    }

    public function getMime($hash){
        $obj = $this->ipfs->cat($hash);
        $hex = strtoupper(self::byte2hex($obj));

        $mimes = [
            'PDF' => '25504446',
            'PNG' => '89504E470D0A1A0A',
            'GIF' => '474946383961',
            'PS' => '25215053',
            'WEBM' => '1A45DFA3',
            'MP3' => 'FFFB',
            'JPEG' => 'FFD8',
        ];

        foreach($mimes as $key=>$value){
            if(strpos($hex,$value) !== FALSE){
                return $key;
            }
        }
    }

    public function setMimeHeader($mimetype){
        $headers = [
            'PDF' => 'application/pdf',
            'PNG' => 'image/png',
            'GIF' => 'image/gif',
            'PS' => 'application/postscript',
            'MP3' => 'audio/mpeg',
            'WEBM' => 'video/webm',
            'JPEG' => 'image/jpeg',
        ];

        // error supressing @headers[$mimetype] because it might fatal err to undefined index
        if(@$headers[$mimetype] != NULL){
            header('Content-Type:'.$headers[$mimetype]);
        }
        // else{
        //     header('Content-Type: application/octet-stream');
        // }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('file.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file;

        $destinationPath = public_path().'/uploads/';
        $file->move($destinationPath,$file->getClientOriginalName());

        $hash = $this->ipfs->add($destinationPath.$file->getClientOriginalName());

        File::delete($destinationPath.$file->getClientOriginalName());
        return redirect('/files/'.$hash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hash)
    {
        self::setMimeHeader(self::getMime($hash));

        echo $this->ipfs->cat($hash);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // No edit.
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // No update.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hash)
    {
        // No destroy.
    }
}
