@extends('layouts.app')

@section('content')
    <div class="upload col-md-5">
        <h3 class="text-center">Upload a file</h3>
        <hr/>
        <form method="POST" action="/files" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="file" type="file"/>
            <button type="submit" class="btn btn-success float-right">Submit</button>
        </form>
    </div>
@stop
